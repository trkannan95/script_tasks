$(document).ready(function(){
		
		
	$('.fileupload input[type="file"]').on("change", function()
    {
		var target=$(this).parent();
		$(this).css('width','0%')
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
        
        if (/^image/.test( files[0].type)){ 
		target.css('box-shadow','0 0 5px #ccc inset')
            var reader = new FileReader(); 
            reader.readAsDataURL(files[0]); 
            
            reader.onloadend = function(){ 
			target.css('background-image', 'url('+this.result+')');
			target.find('i').removeClass('fa-plus').addClass('fa-times');
            }
        }
		else
		{
			target.css('box-shadow','0 0 5px #f00 inset')
		}
    });
	
	
	$('.fileupload i').click(function(){
		$(this).removeClass('fa-times').addClass('fa-plus');
		$(this).parent().find('input[type="file"]').css('width','100%').val('');
		$(this).parent().css('background-image','');
		
		})
		

	
	
	
		

});
